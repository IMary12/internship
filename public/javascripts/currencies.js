$(function() {
	$("#curr").change(function() {
		var currency = $(this);
	    var price = $("#price");
	    price.val("price", parseFloat(currency.val() ).toFixed(2));
	    price.text(parseFloat(currency.val()).toFixed(2));
	});
});